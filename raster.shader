/*
	ラスタースクロール エフェクト by あるる（きのもと 結衣）
	Raster Scroll Effect by Yui Kinomoto @arlez80

	MIT License
*/

shader_type canvas_item;

const float PI = 3.14159265358;

uniform vec2 speed = vec2( 1.0, 0.0 );

uniform vec2 top_power = vec2( 1.0, 0.0 );
uniform vec2 bottom_power = vec2( 1.0, 0.0 );

uniform vec2 scale = vec2( 0.1, 0.0 );


void fragment( )
{
	vec2 power = mix(
		top_power
	,	bottom_power
	,	1.0 - SCREEN_UV.y
	);

	vec2 uv = SCREEN_UV + sin( ( SCREEN_UV.y * PI * scale ) + TIME * speed ) * power;

	COLOR = textureLod( SCREEN_TEXTURE, uv, 0.0 );
}
